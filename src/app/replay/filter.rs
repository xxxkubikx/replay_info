use std::collections::BTreeMap;
use eframe::egui::{Color32, RichText, Ui, Widget};
use sr_libs::utils::card_templates::CardTemplate;
use sr_libs::utils::commands::{command_id_to_name, RequestCommand};
use eframe::egui::DragValue;
use sr_libs::replay_info::{Player, ticks_to_time};

#[derive(Default, Clone, serde::Deserialize, serde::Serialize)]
pub enum Filter {
    Before { before: u32 },
    After { after: u32 },

    Action { id: u16 },

    ByPlayer { player: u32 },
    CloseTo { x: f32, y: f32, distance: f32 },
    PlayCard { card: CardTemplate },
    //WithEntity { entity: u32 }, // does it make sense to follow entity without any visuals?

    Or { f1: Box<Filter>, f2: Box<Filter> },
    And { f1: Box<Filter>, f2: Box<Filter> },
    Not { f: Box<Filter> },
    #[default]
    All,
}

impl Filter {
    pub fn filter(&self, time: u32, action: &RequestCommand, player_by_id: &BTreeMap<u32, usize>, players: &[Player]) -> bool {
        match self {
            Filter::Before { before } => time < *before,
            Filter::After { after } => time > *after,

            Filter::Action { id } => *id == u16::from(action),

            Filter::ByPlayer { player } => {
                let p = super::player_from_action(action, player_by_id, players);
                p.is_none() || Some(*player) == p
            }
            Filter::CloseTo { x, y, distance } =>
                is_in_range(*x, *y, *distance, action),
            Filter::PlayCard { card } =>
                is_with_card(*card, action),
            //Filter::WithEntity { .. } => {}

            Filter::Or { f1, f2 } => f1.filter(time, action, player_by_id, players) || f2.filter(time, action, player_by_id, players),
            Filter::And { f1, f2 } => f1.filter(time, action, player_by_id, players) && f2.filter(time, action, player_by_id, players),
            Filter::Not { f } => !f.filter(time, action, player_by_id, players),
            Filter::All => true,
        }
    }
    pub fn show(&mut self, player_by_id: &BTreeMap<u32, usize>, players: &[Player], ui: &mut Ui) -> bool {
        let mut changed = false;
        ui.group(|ui|{
            let label = self.label();
            let mut selected = label;
            eframe::egui::ComboBox::new("filter", "")
                .selected_text(label)
                .show_ui(ui, |ui|{
                    if ui.selectable_value(&mut selected, Self::BEFORE, Self::BEFORE).changed() {
                        *self = Self::Before { before: u32::MAX };
                        changed = true;
                    }
                    if ui.selectable_value(&mut selected, Self::AFTER, Self::AFTER).changed() {
                        *self = Self::After { after: 0 };
                        changed = true;
                    }
                    if ui.selectable_value(&mut selected, Self::ACTION, Self::ACTION).changed() {
                        *self = Self::Action { id: 4009 };
                        changed = true;
                    }
                    if ui.selectable_value(&mut selected, Self::PLAYER, Self::PLAYER).changed() {
                        *self = Self::ByPlayer { player: player_by_id.iter().next().map(|(player, _)| *player).unwrap_or_default() };
                        changed = true;
                    }
                    if ui.selectable_value(&mut selected, Self::CLOSE_TO, Self::CLOSE_TO).changed() {
                        *self = Self::CloseTo { x: 0., y: 0., distance: 0. };
                        changed = true;
                    }
                    if ui.selectable_value(&mut selected, Self::CARD, Self::CARD).changed() {
                        *self = Self::PlayCard { card: CardTemplate::NotACard };
                        changed = true;
                    }
                    if ui.selectable_value(&mut selected, Self::OR, Self::OR).changed() {
                        let mut old = Self::All;
                        std::mem::swap(self, &mut old);
                        *self = Self::Or { f1: Box::new(old), f2: Box::new(Self::All) };
                        changed = true;
                    }
                    if ui.selectable_value(&mut selected, Self::AND, Self::AND).changed() {
                        let mut old = Self::All;
                        std::mem::swap(self, &mut old);
                        *self = Self::And { f1: Box::new(old), f2: Box::new(Self::All) };
                        changed = true;
                    }
                    if ui.selectable_value(&mut selected, Self::NOT, Self::NOT).changed() {
                        let mut old = Self::All;
                        std::mem::swap(self, &mut old);
                        *self = Self::Not { f: Box::new(old) };
                        changed = true;
                    }
                });
            match self {
                Filter::Before { before } => { changed |= time_edit(before, ui); }
                Filter::After { after } => { changed |= time_edit(after, ui); }

                Filter::Action { id } => {
                    eframe::egui::ComboBox::new("action", "")
                        .selected_text(command_id_to_name(*id))
                        .width(200.)
                        .show_ui(ui, |ui|{
                            for i in 4002..=4044 {
                                if ui.selectable_value(id, i, command_id_to_name(i)).changed() {
                                    changed = true;
                                }
                            }
                        });
                }

                Filter::ByPlayer { player } => {
                    changed |= DragValue::new(player).ui(ui).changed();
                    let combo =
                    eframe::egui::ComboBox::new("player", "");
                    let combo = if let Some(i) = player_by_id.get(player) {
                        combo.selected_text(&players[*i].name)
                    } else {
                        combo
                    };
                    combo
                        .show_ui(ui, |ui|{
                            for (p, i) in player_by_id.iter() {
                                if ui.selectable_value(player, *p, &players[*i].name).changed(){
                                    changed = true;
                                }
                            }
                        });
                }
                Filter::CloseTo { x, y, distance } => {
                    ui.horizontal(|ui|{
                        ui.label("x:");
                        changed |= DragValue::new(x).ui(ui).changed();
                        ui.label("y:");
                        changed |= DragValue::new(y).ui(ui).changed();
                        ui.label(" distance:");
                        changed |= DragValue::new(distance).ui(ui).changed();
                    });
                }
                Filter::PlayCard { card } => {
                    let mut c = card.base_id();
                    if DragValue::new(&mut c).ui(ui).changed() {
                        *card = CardTemplate::from_u32(c);
                        changed = true;
                    }
                    eframe::egui::ComboBox::new("card", "")
                        .selected_text(format!("{card:?}"))
                        .width(200.)
                        .show_ui(ui, |ui|{
                            use sr_libs::utils::card_templates::IntoEnumIterator;
                            for ct in CardTemplate::iter() {
                                if ui.selectable_value(card, ct, format!("{ct:?}")).changed(){
                                    changed = true;
                                }
                            }
                        });
                }

                Filter::Or { f1, f2 } => {
                    changed |= f1.show(player_by_id, players, ui);
                    ui.push_id("f2", |ui|{
                        changed |= f2.show(player_by_id, players, ui);
                    });
                }
                Filter::And { f1, f2 } => {
                    changed |= f1.show(player_by_id, players, ui);
                    ui.push_id("f2", |ui|{
                        changed |= f2.show(player_by_id, players, ui);
                    });
                }
                Filter::Not { f } => {
                    changed |= f.show(player_by_id, players, ui);
                }
                Filter::All => {}
            }
            if let Filter::All = self {

            } else if ui.button(RichText::new("X").color(Color32::RED)).clicked() {
                *self = Self::All;
                changed = true;
            }
        });
        changed
    }
    fn label(&self) -> &'static str {
        match self {
            Filter::Before { .. } => Self::BEFORE,
            Filter::After { .. } => Self::AFTER,
            Filter::Action { .. } => Self::ACTION,
            Filter::ByPlayer { .. } => Self::PLAYER,
            Filter::CloseTo { .. } => Self::CLOSE_TO,
            Filter::PlayCard { .. } => Self::CARD,
            Filter::Or { .. } => Self::OR,
            Filter::And { .. } => Self::AND,
            Filter::Not { .. } => Self::NOT,
            Filter::All => Self::ALL,
        }
    }

    const BEFORE : &'static str = "Before";
    const AFTER : &'static str = "After";
    const ACTION : &'static str = "Action";
    const PLAYER : &'static str = "Player";
    const CLOSE_TO : &'static str = "Close to";
    const CARD : &'static str = "Card";
    const OR : &'static str = "OR";
    const AND : &'static str = "AND";
    const NOT : &'static str = "NOT";
    const ALL : &'static str = "ALL";
}

fn time_edit(ticks: &mut u32, ui: &mut Ui) -> bool {
    ui.horizontal(|ui|{
        ui.label("total ticks: ");
        DragValue::new(ticks).ui(ui);
    });
    let (mut h, mut m, mut s, mut t) = ticks_to_time(*ticks);
    ui.horizontal(|ui|{
        ui.label("hh:mm:ss.t ");
        let changed = DragValue::new(&mut h).ui(ui).changed();
        ui.label(":");
        let changed = changed | DragValue::new(&mut m).clamp_range(0..=59).ui(ui).changed();
        ui.label(":");
        let changed = changed | DragValue::new(&mut s).clamp_range(0..=59).ui(ui).changed();
        ui.label(".");
        let changed = changed | DragValue::new(&mut t).clamp_range(0..=9).ui(ui).changed();
        if changed {
            *ticks = t as u32 + 10 * s as u32 + 600 * m as u32 + 36000 * h as u32;
        }
        changed
    }).inner
}

fn is_with_card(ct: CardTemplate, action: &RequestCommand) -> bool {
    card(action)
        .map(|c| CardTemplate::from_u32(c) == ct)
        .unwrap_or_default()
}

pub fn card(action: &RequestCommand) -> Option<u32> {
    match action {
        RequestCommand::ActivateCheat { .. } => None,
        RequestCommand::BarrierGateToggle { .. } => None,
        RequestCommand::BarrierSetBuildOrRepair { .. } => None,
        RequestCommand::BuildHouse { card, .. } => Some(*card),
        RequestCommand::CastSpellGod { card, .. } => Some(*card),
        RequestCommand::CastSpellGodMulti { card, .. } => Some(*card),
        RequestCommand::CastSpellEntity { .. } => None,
        RequestCommand::ConstructionRepair { .. } => None,
        RequestCommand::Desync { .. } => None,
        RequestCommand::GroupAttack { .. } => None,
        RequestCommand::GroupEnterExitWall { .. } => None,
        RequestCommand::GroupGoto { .. } => None,
        RequestCommand::GroupHoldPosition { .. } => None,
        RequestCommand::GroupKillEntity { .. } => None,
        RequestCommand::SuperWeaponShadowSetBombBuilding { .. } => None,
        RequestCommand::GroupLootTarget { .. } => None,
        RequestCommand::GroupSacrifice { .. } => None,
        RequestCommand::GroupStopJob { .. } => None,
        RequestCommand::ModeChange { .. } => None,
        RequestCommand::Morph { .. } => None,
        RequestCommand::PlayerLoot { .. } => None,
        RequestCommand::PlayerLootResult { .. } => None,
        RequestCommand::PlayerSurrender { .. } => None,
        RequestCommand::PortalDefineExitPoint { .. } => None,
        RequestCommand::PortalRemoveExitPoint { .. } => None,
        RequestCommand::TunnelMakeExitPoint { .. } => None,
        RequestCommand::GroupUsePortal { .. } => None,
        RequestCommand::PowerSlotBuild { .. } => None,
        RequestCommand::ProduceSquad { card, .. } => Some(*card),
        RequestCommand::ScriptCheat { .. } => None,
        RequestCommand::ScriptVariable { .. } => None,
        RequestCommand::ScriptGoal { .. } => None,
        RequestCommand::SquadRefill { .. } => None,
        RequestCommand::TokenSlotBuild { .. } => None,
        RequestCommand::UiInfo { .. } => None,
        RequestCommand::ServerReplayEndOld  { } => None,
        RequestCommand::SrAddition { .. } => None,
    }
}

fn is_in_range(tx: f32, ty: f32, distance: f32, action: &RequestCommand) -> bool {
    const DEFAULT: bool = false;
    match action {
        RequestCommand::ActivateCheat { x, y, .. } => {
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::BarrierGateToggle { .. } => {
            DEFAULT
        }
        RequestCommand::BarrierSetBuildOrRepair { .. } => {
            DEFAULT
        }
        RequestCommand::BuildHouse { x, y, .. } => {
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::CastSpellGod { target, .. } => {
            let x = target.targets[0].x;
            let y = target.targets[0].y;
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::CastSpellGodMulti { target, .. } => {
            let x = target.targets[0].x;
            let y = target.targets[0].y;
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::CastSpellEntity { .. } => {
            DEFAULT
        }
        RequestCommand::ConstructionRepair { .. } => {
            DEFAULT
        }
        RequestCommand::Desync { .. } => {
            DEFAULT
        }
        RequestCommand::GroupAttack { target, .. } => {
            let distance_sq = distance * distance;
            target.targets
                  .iter()
                  .any(|target| (tx - target.x) * (tx - target.x) + (ty - target.y) * (ty - target.y) <= distance_sq)
        }
        RequestCommand::GroupEnterExitWall { .. } => {
            DEFAULT
        }
        RequestCommand::GroupGoto { positions, .. } => {
            let distance_sq = distance * distance;
            positions
                .iter()
                .any(|p| (tx - p.0) * (tx - p.0) + (ty - p.1) * (ty - p.1) <= distance_sq)
        }
        RequestCommand::GroupHoldPosition { .. } => {
            DEFAULT
        }
        RequestCommand::GroupKillEntity { .. } => {
            DEFAULT
        }
        RequestCommand::SuperWeaponShadowSetBombBuilding { x, y, .. } => {
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::GroupLootTarget { .. } => {
            DEFAULT
        }
        RequestCommand::GroupSacrifice { .. } => {
            DEFAULT
        }
        RequestCommand::GroupStopJob { .. } => {
            DEFAULT
        }
        RequestCommand::ModeChange { .. } => {
            DEFAULT
        }
        RequestCommand::Morph { target, .. } => {
            let x = target.targets[0].x;
            let y = target.targets[0].y;
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::PlayerLoot { .. } => {
            DEFAULT
        }
        RequestCommand::PlayerLootResult { .. } => {
            DEFAULT
        }
        RequestCommand::PlayerSurrender { .. } => {
            DEFAULT
        }
        RequestCommand::PortalDefineExitPoint { x, y, .. } => {
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::PortalRemoveExitPoint { .. } => {
            DEFAULT
        }
        RequestCommand::TunnelMakeExitPoint { .. } => {
            DEFAULT
        }
        RequestCommand::GroupUsePortal { .. } => {
            DEFAULT
        }
        RequestCommand::PowerSlotBuild { .. } => {
            DEFAULT
        }
        RequestCommand::ProduceSquad { x, y, .. } => {
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::ScriptCheat { .. } => {
            DEFAULT
        }
        RequestCommand::ScriptVariable { .. } => {
            DEFAULT
        }
        RequestCommand::ScriptGoal { .. } => {
            DEFAULT
        }
        RequestCommand::SquadRefill { .. } => {
            DEFAULT
        }
        RequestCommand::TokenSlotBuild { .. } => {
            DEFAULT
        }
        RequestCommand::UiInfo { target, .. } => {
            let x = target.targets[0].x;
            let y = target.targets[0].y;
            (tx - x) * (tx - x) + (ty - y) * (ty - y) <= distance * distance
        }
        RequestCommand::ServerReplayEndOld { } => {
            DEFAULT
        }
        RequestCommand::SrAddition { .. } => {
            DEFAULT
        }
    }
}
