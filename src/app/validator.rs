use std::collections::BTreeMap;
use std::ops::RangeInclusive;
use eframe::egui::{CollapsingHeader, DragValue, Label, RichText, Sense, Ui, Widget};
use eframe::epaint::Color32;
use sr_libs::cff::card::CardType;
use sr_libs::replay_info::Player;
use sr_libs::utils::card_templates::{IntoEnumIterator};
use sr_libs::utils::maps::Maps;
use crate::app::replay::filter::Filter;
use crate::app::replay::{iter_actions, ReplayUiData};

#[derive(Default, serde::Deserialize, serde::Serialize)]
pub struct ReplayValidatorConfig {
    rules: Vec<(String, RuleDefinition)>,
}

#[derive(Clone)]
pub struct ValidatedReplayUiData {
    data: ReplayUiData,
    rules: Vec<Rule>
}

#[derive(Clone)]
pub struct  Rule {
    name: String,
    definition: RuleDefinition,
    result: Result<RuleResult, RuleResult>,
}

#[derive(Default, Clone, serde::Deserialize, serde::Serialize)]
pub enum RuleDefinition {
    #[default]
    AlwaysPassed,
    Map { map : Maps },
    Difficulty { difficulty: u8 },
    ActionCount { filter: Filter, count: RangeInclusive<u32>},
    SumCost { multiplier: PerTypeMultiplier, sum: RangeInclusive<f32> },
    Version { game: u32, files: u32 },
}

#[derive(Default, Clone, serde::Deserialize, serde::Serialize)]
pub struct PerTypeMultiplier {
    pub squad: f32,
    pub spell: f32,
    pub building: f32,
}

#[derive(Debug)]
#[derive(Clone)]
pub enum RuleResult {
    None,

    Map(Maps),
    Difficulty(u8),
    Count(u32),
    Sum(f32),
    //Card(CardTemplate),

    //Multiple(Vec<RuleResult>),
    NoGameDataLoaded,
    Version{ game: u32, files: u32 },
}

impl ReplayValidatorConfig {
    pub fn new() -> Self {
        Self {
            rules: vec![],
        }
    }
    pub fn show(&mut self, ui: &mut Ui) {
        // TODO
        let todo_player_by_id = &BTreeMap::default();
        let todo_players = &[];
        ui.horizontal(|ui|{
            ui.label("All validations");
            if ui.button("to clipboard").clicked() {
                ui.ctx().output_mut(|out|{
                    out.copied_text = serde_json::to_string(self).unwrap();
                });
            }
            ui.label("load: ");
            let mut text = String::new();
            if ui.text_edit_singleline(&mut text).changed() {
                if let Ok(validator) = serde_json::from_str(&text) {
                    *self = validator;
                }
            }
        });
        eframe::egui::ComboBox::new("LOAD_PRESET", "preset")
            .width(ui.available_width().min(200.))
            .show_ui(ui, |ui|{
                if ui.selectable_value(&mut false, true, "Empty").clicked() {
                    *self = ReplayValidatorConfig { rules: vec![] };
                }
                if ui.selectable_value(&mut false, true, "Old School Efficiency").clicked() {
                    *self = ReplayValidatorConfig {
                        rules: vec![
                            ("no abilities".to_string(), RuleDefinition::ActionCount {
                                filter: Filter::Or {
                                    f1: Box::new(Filter::Action { id: 4014 }), // CastSpellEntity
                                    f2: Box::new(Filter::Or {
                                        f1: Box::new(Filter::Action { id: 4034 }), // ModeChange
                                        f2: Box::new(Filter::Action { id: 4044 }), //Morph
                                    }),
                                },
                                count: 0..=0,
                            }),
                            ("score".to_string(), RuleDefinition::SumCost {
                                multiplier: PerTypeMultiplier{
                                    squad: 2.0,
                                    spell: 1.0,
                                    building: 1.5,
                                },
                                sum: 0.0..=f32::INFINITY
                            }),
                            ("map".to_string(), RuleDefinition::Map { map: Maps::EncountersWithTwilight }),
                            ("difficulty".to_string(), RuleDefinition::Difficulty { difficulty: 1 }),
                            ("version".to_string(), RuleDefinition::Version { game: 261, files: 400042 }),
                            ("under 45 minutes".to_string(), RuleDefinition::ActionCount {
                                filter: Filter::After { after: 45 * 60 * 10 },
                                count: 0..=0,
                            })
                        ],
                    }
                }
            });
        let mut remove = None;
        for (i, (name, rule)) in self.rules.iter_mut().enumerate() {
            ui.push_id(i, |ui| {
                ui.group(|ui| {
                    ui.horizontal(|ui| {
                        ui.label("Name: ");
                        ui.text_edit_singleline(name);
                        if ui.button(RichText::new("-").color(Color32::RED)).clicked() {
                            remove = Some(i);
                        }
                    });
                    rule.show(todo_player_by_id, todo_players, ui);
                });
            });
        }
        if let Some(i) = remove {
            self.rules.remove(i);
        }
        if ui.button(RichText::new("+").color(Color32::GREEN)).clicked() {
            self.rules.push(("New rule".to_owned(), RuleDefinition::AlwaysPassed ))
        }
    }
    pub fn validate(&self, data: ReplayUiData, game_data: &Option<sr_libs::cff::GameDataCffFile>) -> ValidatedReplayUiData {
        ValidatedReplayUiData::new(data, self.rules.clone(), game_data)
    }
}

impl ValidatedReplayUiData {
    fn new(data: ReplayUiData, mut rules: Vec<(String, RuleDefinition)>, game_data: &Option<sr_libs::cff::GameDataCffFile>) -> Self {
        let rules =
            rules
                .drain(0..)
                .map(|(name, r)| Rule::new(name, r, &data, game_data))
                .collect();
        Self {
            data,
            rules,
        }
    }
    pub fn show(&mut self, ui: &mut Ui) {
        CollapsingHeader::new("validation").default_open(true).show(ui, |ui|{
            for (i, rule) in self.rules.iter().enumerate() {
                ui.push_id(i, |ui|{
                    ui.group(|ui|{
                        if rule.show(ui) {
                            match &rule.definition {
                                RuleDefinition::AlwaysPassed => {}
                                RuleDefinition::Map { .. } => {}
                                RuleDefinition::Difficulty { .. } => {}
                                RuleDefinition::ActionCount { filter, .. } => {
                                    self.data.set_filter(filter.clone());
                                }
                                RuleDefinition::SumCost { .. } => {
                                    self.data.set_filter(Filter::Or {
                                        f1: Box::new(Filter::Action { id: 4012 }), // BuildHouse
                                        f2: Box::new(Filter::Or {
                                            f1: Box::new(Filter::Action { id: 4010 }), // CastSpellGod
                                            f2: Box::new(Filter::Or {
                                                f1: Box::new(Filter::Action { id: 4011 }), // CastSpellGodMulti
                                                f2: Box::new(Filter::Action { id: 4009 }), // ProduceSquad
                                            }),
                                        }),
                                    });
                                }
                                RuleDefinition::Version { .. } => {}
                            }
                        }
                    });
                });
            }
        });
        self.data.show(ui);
    }
}

impl Rule {
    fn new(name: String, definition: RuleDefinition, data: &ReplayUiData, game_data: &Option<sr_libs::cff::GameDataCffFile>) -> Self {
        let result = definition.validate(data, game_data);
        Self {
            name,
            definition,
            result,
        }
    }
    pub fn show(&self, ui: &mut Ui) -> bool {
        let sense =
            match self.definition {
                RuleDefinition::AlwaysPassed |
                RuleDefinition::Map { .. } |
                RuleDefinition::Version { .. } |
                RuleDefinition::Difficulty { .. } => Sense::hover(),
                RuleDefinition::SumCost { .. } |
                RuleDefinition::ActionCount { .. } => Sense::click(),
            };
        ui.horizontal(|ui|{
            let clicked = Label::new(&self.name).sense(sense).ui(ui).clicked();
            let color = if self.result.is_ok() { Color32::GREEN } else { Color32::RED };
            let result = match &self.result {
                Ok(r) => r,
                Err(r) => r,
            };
            clicked | Label::new(RichText::new(format!("{:?}", result)).color(color)).sense(sense).ui(ui).clicked()
        }).inner
    }
}

impl RuleDefinition {
    pub fn validate(&self, data: &ReplayUiData, game_data: &Option<sr_libs::cff::GameDataCffFile>) -> Result<RuleResult, RuleResult> {
        match self {
            RuleDefinition::AlwaysPassed => Ok(RuleResult::None),
            RuleDefinition::Map { map } =>
                if data.replay.map == *map {
                    Ok(RuleResult::Map(data.replay.map))
                } else {
                    Err(RuleResult::Map(data.replay.map))
                }
            RuleDefinition::Difficulty { difficulty } =>
                if data.replay.difficulty == *difficulty {
                    Ok(RuleResult::Difficulty(data.replay.difficulty))
                } else {
                    Err(RuleResult::Difficulty(data.replay.difficulty))
                }
            RuleDefinition::ActionCount { filter, count } => {
                let real_count =
                    iter_actions(&data.replay)
                        .filter(|(t, a)| filter.filter(*t, a, &data.player_by_id, &data.replay.players))
                        .count() as u32;
                if count.contains(&real_count) {
                    Ok(RuleResult::Count(real_count))
                } else {
                    Err(RuleResult::Count(real_count))
                }
            }
            RuleDefinition::SumCost { multiplier, sum } => {
                if let Some(game_data) = game_data {
                    let real_sum =
                        iter_actions(&data.replay)
                            .map(|(_, a)|
                                if let Some(c) = crate::app::replay::filter::card(a) {
                                    cost(c, multiplier, game_data)
                                } else {
                                    0.0
                                }
                            )
                            .sum();
                    if sum.contains(&real_sum) {
                        Ok(RuleResult::Sum(real_sum))
                    } else {
                        Err(RuleResult::Sum(real_sum))
                    }
                } else {
                    Err(RuleResult::NoGameDataLoaded)
                }
            }
            RuleDefinition::Version { game, files } => {
                if *game == data.replay.game_version && *files == data.replay.client_files_version {
                    Ok(RuleResult::Version { game: data.replay.game_version, files: data.replay.client_files_version })
                } else {
                    Err(RuleResult::Version { game: data.replay.game_version, files: data.replay.client_files_version })
                }
            }
        }
    }
    fn show(&mut self, player_by_id: &BTreeMap<u32, usize>, players: &[Player], ui: &mut Ui) {
        let label = self.label();
        let mut selected = label;
        eframe::egui::ComboBox::new("rule", "")
            .width(200.)
            .selected_text(label)
            .show_ui(ui, |ui|{
                if ui.selectable_value(&mut selected, Self::ALWAYS_PASSED, Self::ALWAYS_PASSED).changed() {
                    *self = Self::AlwaysPassed {  };
                }
                if ui.selectable_value(&mut selected, Self::MAP, Self::MAP).changed() {
                    *self = Self::Map { map: Maps::Introduction  };
                }
                if ui.selectable_value(&mut selected, Self::DIFFICULTY, Self::DIFFICULTY).changed() {
                    *self = Self::Difficulty { difficulty: 1  };
                }
                if ui.selectable_value(&mut selected, Self::ACTION_COUNT, Self::ACTION_COUNT).changed() {
                    *self = Self::ActionCount { filter: Filter::All, count: 0..=u32::MAX  };
                }
            });
        match self {
            RuleDefinition::AlwaysPassed => {}
            RuleDefinition::Map { map } => {
                eframe::egui::ComboBox::new("map", "map")
                    .width(200.)
                    .selected_text(map.name())
                    .show_ui(ui, |ui|{
                        for next in Maps::iter() {
                            ui.selectable_value(map, next, next.name());
                        }
                    });
            }
            RuleDefinition::Difficulty { difficulty } => {
                DragValue::new(difficulty).clamp_range(1..=15).ui(ui);
            }
            RuleDefinition::ActionCount { filter, count } => {
                filter.show(player_by_id, players, ui);
                ui.horizontal(|ui|{
                    ui.label("min: ");
                    let mut start = *count.start();
                    let mut end = *count.end();
                    if DragValue::new(&mut start).clamp_range(0..=end).ui(ui).changed() {
                        *count = start..=end;
                    }
                    ui.label(" max: ");
                    if DragValue::new(&mut end).clamp_range(start..=u32::MAX).ui(ui).changed() {
                        *count = start..=end;
                    }
                });
            }
            RuleDefinition::SumCost { multiplier, sum } => {
                ui.horizontal(|ui|{
                    ui.label("squad multiplier");
                    DragValue::new(&mut multiplier.squad).clamp_range(0.0..=100.0).ui(ui);
                });
                ui.horizontal(|ui|{
                    ui.label("spell multiplier");
                    DragValue::new(&mut multiplier.spell).clamp_range(0.0..=100.0).ui(ui);
                });
                ui.horizontal(|ui|{
                    ui.label("building multiplier");
                    DragValue::new(&mut multiplier.building).clamp_range(0.0..=100.0).ui(ui);
                });
                ui.horizontal(|ui| {
                    let mut start = *sum.start();
                    let mut end = *sum.end();
                    if DragValue::new(&mut start).clamp_range(0.0..=end).ui(ui).changed() {
                        *sum = start..=end;
                    }
                    ui.label(" max: ");
                    if DragValue::new(&mut end).clamp_range(start..=f32::MAX).ui(ui).changed() {
                        *sum = start..=end;
                    }
                });
            }
            RuleDefinition::Version { game, files } => {
                ui.horizontal(|ui|{
                    ui.label("game");
                    DragValue::new(game).clamp_range(253..=300).ui(ui);
                });
                ui.horizontal(|ui|{
                    ui.label("files");
                    DragValue::new(files).clamp_range(0..=u32::MAX).ui(ui);
                });
            }
        }
    }
    fn label(&self) -> &'static str {
        match self {
            RuleDefinition::AlwaysPassed => Self::ALWAYS_PASSED,
            RuleDefinition::Map { .. } => Self::MAP,
            RuleDefinition::Difficulty { .. } => Self::DIFFICULTY,
            RuleDefinition::ActionCount { .. } => Self::ACTION_COUNT,
            RuleDefinition::SumCost { .. } => Self::SUM_COST,
            RuleDefinition::Version { .. } => Self::VERSION,
        }
    }

    const ALWAYS_PASSED: &'static str = "ALWAYS OK";
    const MAP : &'static str = "Map";
    const DIFFICULTY: &'static str = "Difficulty";
    const ACTION_COUNT: &'static str = "Action filter";
    const SUM_COST: &'static str = "Sum cost";
    const VERSION: &'static str = "Version";
}

fn cost(card: u32, multiplier: &PerTypeMultiplier, game_data: &sr_libs::cff::GameDataCffFile) -> f32 {
    game_data.cards.entities.iter().find(|(id, _)| **id == card )
        .map(|(_,c)|
            match c.r#type {
                CardType::Unknown => unreachable!(),
                CardType::Building =>
                    game_data.buildings.entities.iter().find(|(id, _)| **id == c.db_entity_id)
                        .map(|(_, e)| e.production_power).unwrap() as f32 * multiplier.building,
                CardType::Squad =>
                    game_data.squads.entities.iter().find(|(id, _)| **id == c.db_entity_id)
                             .map(|(_, e)| e.production_power).unwrap() as f32 * multiplier.squad,
                CardType::Spell =>
                    game_data.spells.entities.iter().find(|(id, _)| **id == c.db_entity_id)
                             .map(|(_, e)| e.production_power).unwrap() as f32 * multiplier.spell,
            }
        ).unwrap()
}
