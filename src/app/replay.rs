pub mod filter;

use std::collections::BTreeMap;
use eframe::egui;
use eframe::egui::{Color32, Id};
use egui::Ui;

use sr_libs::replay_info::*;
use sr_libs::utils::card_templates::CardTemplate;
use sr_libs::utils::commands::{ParsingError, RequestCommand, SrAdditionData};

#[derive(Clone)]
pub struct ReplayUiData {
    pub replay: ReplayInfo,
    pub time: String,
    pub player_by_id: BTreeMap<u32, usize>,
    show_players_in_actions: bool,
    actions_filter: filter::Filter,
    filter_text: String,
    filter_text_valid: bool,
}

pub fn iter_actions(replay: &ReplayInfo) -> impl Iterator<Item=(u32, &RequestCommand)> {
    replay.steps
          .iter()
          .flat_map(|(t,a)| a.iter().map(|a| (*t, a)))
}

impl ReplayUiData {
    pub fn new(bytes: &[u8]) -> Result<ReplayUiData, ParsingError> {
        info(bytes)
            .map(|replay|{
                let time = replay.time();
                let least_player_id =
                    iter_actions(&replay)
                        .filter_map(|(_, a)| player_from_action(a, &BTreeMap::new(), &replay.players))
                        .min()
                        .unwrap_or_default();
                let mut next_player_id = least_player_id;
                let player_by_id =
                    replay.players
                          .iter()
                          .enumerate()
                          .filter(|(_, p)| p.team == 4 || p.team == 5 || p.team == 6)
                          .map(|(i, _)| {
                              let id = next_player_id;
                              next_player_id += 1;
                              (id, i)
                          })
                          .collect();
                let show_players_in_actions =
                    iter_actions(&replay)
                        .filter_map(|(_, a)| player_from_action(a, &player_by_id, &replay.players))
                        .any(|p| p != least_player_id);
                ReplayUiData{
                    replay,
                    time,
                    player_by_id,
                    show_players_in_actions,
                    actions_filter: filter::Filter::All,
                    filter_text: String::new(),
                    filter_text_valid: true,
                }
            })
    }
    pub fn set_filter(&mut self, filter: filter::Filter) {
        self.filter_text_valid = true;
        self.filter_text = serde_json::to_string(&filter).unwrap();
        self.actions_filter = filter;
    }

    pub fn show(&mut self, ui: &mut Ui) {
        egui::CollapsingHeader::new("basic info").default_open(true).show(ui, |ui|{
            egui::Grid::new("Errors").show(ui, |ui|{
                ui.label(egui::RichText::new("game_version").strong());
                ui.label(format!("{}", self.replay.game_version));
                ui.end_row();
                ui.label(egui::RichText::new("client_files_version").strong());
                ui.label(format!("{}", self.replay.client_files_version));
                ui.end_row();
                ui.label(egui::RichText::new("map").strong());
                ui.label(format!("{:?}", self.replay.map));
                ui.end_row();
                ui.label(egui::RichText::new("difficulty").strong());
                ui.label(format!("{}", self.replay.difficulty));
                ui.end_row();
                ui.label(egui::RichText::new("seed").strong());
                ui.label(format!("{}", self.replay.seed));
                ui.end_row();
                ui.label(egui::RichText::new("time").strong());
                ui.label(&self.time);
                ui.end_row();
            });
        });
        egui::CollapsingHeader::new("players").default_open(false).show(ui, |ui|{
            for (i, player) in self.replay.players.iter().enumerate() {
                let is_relevant = match player.player_type {
                    PlayerType::Human => true,
                    PlayerType::AiBot => true,
                    PlayerType::Monsters => false,
                    PlayerType::Any => true,//false when?
                    PlayerType::EmptySlot => false,
                };
                egui::CollapsingHeader::new(&player.name)
                    .default_open(is_relevant).id_source(Id::new((player.team, player.position)))
                    .show(ui, |ui| {
                    egui::Grid::new("team info").show(ui, |ui|{
                        ui.label(egui::RichText::new("team").strong());
                        ui.label(format!("{}", player.team));
                        ui.end_row();
                        ui.label(egui::RichText::new("position").strong());
                        ui.label(format!("{}", player.position));
                        ui.end_row();
                        ui.label(egui::RichText::new("guessed ID").strong());
                        if let Some((id,_)) = self.player_by_id.iter().find(|(_, index)| **index == i){
                            ui.label(format!("{id}"));
                        } else {
                            ui.label("-");
                        }
                        ui.end_row();
                    });
                    ui.collapsing("played cards (guessed)", |ui|{
                        for played_card in player.guessed_played_cards.iter() {
                            ui.label(format!("{:?} times: {}", played_card.card, played_card.played_count));
                        }
                    });
                    egui::CollapsingHeader::new("deck cards (wrong in client replays!")
                        .show(ui, |ui| {
                            for card in &player.cards {
                                ui.label(format!("{:?}", card.card));
                            }
                        });
                });
            }
        });
        if let Some(stats) = self.replay.steps.iter()
            .find_map(|(t, c)| if *t != u32::MAX { None } else {
                for c in c.iter() {
                    match c {
                        RequestCommand::SrAddition { data: SrAdditionData::BotsStats { stats } } => {
                            return Some(stats);
                        }
                        _ => {}
                    }
                }
                None
            }) {
            ui.collapsing("bot stats", |ui|{
                for stat in stats {
                    ui.group(|ui|{
                        ui.horizontal(|ui|{
                            ui.label("name: ");
                            ui.label(&stat.name);
                        });
                        ui.label(format!("time spent: {:?}", std::time::Duration::from_millis(stat.time_spent_ms as u64)));
                        ui.label(format!("actions: {}", stat.actions));
                        ui.label(format!("invalid actions: {}", stat.invalid_actions));
                        ui.label(format!("errors: {}", stat.errors));
                    });
                }
            });
        }
        egui::CollapsingHeader::new("steps").default_open(false).show(ui, |ui|{
            ui.checkbox(&mut self.show_players_in_actions, "show players in actions");
            ui.collapsing("filter", |ui|{
                if self.filter_text_valid {
                    // TODO red text
                }
                if ui.text_edit_singleline(&mut self.filter_text).changed() {
                    if let Ok(filter) = serde_json::from_str(&self.filter_text){
                        self.filter_text_valid = true;
                        self.actions_filter = filter;
                    } else {
                        self.filter_text_valid = false;
                    }
                }
                if self.actions_filter.show(&self.player_by_id, &self.replay.players, ui) {
                    self.filter_text = serde_json::to_string(&self.actions_filter).unwrap();
                }
            });
            egui::ScrollArea::vertical().show(ui, |ui|{
                egui::Grid::new("Errors").show(ui, |ui|{
                    ui.label(egui::RichText::new("time").strong());
                    let player_by_id = if self.show_players_in_actions {
                        ui.label(egui::RichText::new("player").strong());
                        Some(&self.player_by_id)
                    } else {
                        None
                    };
                    ui.label(egui::RichText::new("action").strong());
                    ui.label(egui::RichText::new("details").strong());
                    ui.end_row();
                    let filtered_actions =
                        iter_actions(&self.replay)
                            .filter(|(_, a)| filter_sr_actions(a))
                            .filter(|(t,a)| self.actions_filter.filter(*t,a, &self.player_by_id, &self.replay.players));
                    for (time, action) in filtered_actions {
                        let time = ticks_to_time_string(time);
                        ui.label(&time);
                        if let Some(player_by_id) = &player_by_id {
                            if let Some(player) = player_from_action(action, &self.player_by_id, &self.replay.players){
                                if let Some(i) = player_by_id.get(&player) {
                                    ui.strong(&self.replay.players[*i].name);
                                } else {
                                    ui.strong("?");
                                }
                            } else {
                                ui.label("?...");
                            }
                        }
                        nice_print_action(action, ui);
                        //ui.label(format!("{action:?}"));
                        ui.end_row();
                    }
                });
            });
        });
    }
}

fn filter_sr_actions(a: &&RequestCommand) -> bool {
    match a {
        RequestCommand::SrAddition{data} => {
            match data {
                SrAdditionData::Unknown { .. } => false,
                SrAdditionData::ServerReplayEnd { .. } => false,
                SrAdditionData::StateHashes { .. } => false,
                SrAdditionData::GamePaused { .. } => false,
                SrAdditionData::Goal { .. } => false,
                SrAdditionData::MatchModifier { .. } => false,
                SrAdditionData::BotsStats { .. } => false,
            }
        }
        _ => true,
    }
}

fn nice_print_action(action: &RequestCommand,  ui: &mut Ui) {
    match action {
        RequestCommand::ActivateCheat { player: _player, cheat, x, y, flag, v1, v2  } => {
            ui.strong("Cheat");
            ui.label(format!(" {cheat} {x} {y} {flag} {v1} {v2}"));
        }
        RequestCommand::BarrierGateToggle { player: _player, barrier } => {
            ui.strong("Toggle gate");
            ui.label(format!(" barrier: {barrier}"));
        }
        RequestCommand::BarrierSetBuildOrRepair { player: _player, barrier, action } => {
            ui.strong("Repair barrier");
            ui.label(format!(" barrier: {barrier} cancel: {}", *action != 0));
        }
        RequestCommand::BuildHouse { card, player: _player, building_id, x, y, z, angle, tag, played_count } => {
            ui.strong(format!("PLAY {:?}", CardTemplate::from_u32(*card)));
            ui.label(format!(" card: {card} building: {building_id} x/y/a: {x}/{y}/{z}/{angle} tag: {tag} played_count: {played_count}"));
        }
        RequestCommand::CastSpellGod { card, player: _player, card_position, tag, played_count, target } => {
            ui.strong(format!("PLAY {:?}", CardTemplate::from_u32(*card)));
            let t = &target.targets[0];
            ui.label(format!(" card: {card} x/y: {}/{} tag: {tag} card_position: {card_position} played_count: {played_count}", t.x, t.y));
        }
        RequestCommand::CastSpellGodMulti { card, player: _player, card_position, tag, played_count, target } => {
            ui.strong(format!("PLAY {:?}", CardTemplate::from_u32(*card)));
            let t0 = &target.targets[0];
            let t1 = &target.targets[1];
            ui.label(format!(" card: {card} x/y: {}/{} -> {}/{} tag: {tag} card_position: {card_position} played_count: {played_count}", t0.x, t0.y, t1.x, t1.y));
        }
        RequestCommand::CastSpellEntity { player: _player, source, spell, target } => {
            ui.strong("CastSpellEntity");
            let t = &target.targets[0];
            ui.label(format!(" source: {source} spell: {spell} x/y: {}/{}", t.x, t.y));
        }
        RequestCommand::ConstructionRepair { player: _player, construction, cancel_repair } => {
            ui.strong("Construction Repair");
            ui.label(format!(" construction: {construction} cancel_repair: {cancel_repair}"));
        }
        RequestCommand::Desync { account, reason } => {
            ui.strong("Desync");
            ui.label(format!(" account: {account} reason: {reason}")); // TODO map to _player name
        }
        RequestCommand::GroupAttack { player: _player, squads, target, force_attack } => {
            ui.strong("GroupAttack");
            let targets : Vec<u32> = target.targets.iter().map(|t| t.uid).collect();
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" force_attack: {force_attack} squads: {squads:?} targets: {targets:?}"));
            });
        }
        RequestCommand::GroupEnterExitWall { player: _player, squads, barrier, exit_wall } => {
            ui.strong("GroupEnterExitWall");
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" barrier: {barrier} exit_wall: {exit_wall} squads: {squads:?}"));
            });
        }
        RequestCommand::GroupGoto { player: _player, squads, positions, walk_mode, run_or_orientation, orientation } => {
            ui.strong("GroupGoto");
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" walk_mode: {walk_mode} run_or_orientation: {run_or_orientation} orientation: {orientation} squads: {squads:?} positions: {positions:?}"));
            });
        }
        RequestCommand::GroupHoldPosition { player: _player, squads } => {
            ui.strong("GroupHoldPosition");
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" squads: {squads:?}"));
            });
        }
        RequestCommand::GroupKillEntity { player: _player, entities } => {
            ui.strong("GroupKillEntity");
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" entities: {entities:?}"));
            });
        }
        RequestCommand::SuperWeaponShadowSetBombBuilding { player: _player, building, x, y } => {
            ui.strong("SuperWeaponShadowSetBombBuilding");
            ui.label(format!(" building: {building} x/y {x}/{y}"));
        }
        RequestCommand::GroupLootTarget { player: _player, squads, loot_target } => {
            ui.strong("GroupLootTarget");
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" loot_target: {loot_target} squads: {squads:?}"));
            });
        }
        RequestCommand::GroupSacrifice { squads, target, player: _player } => {
            ui.strong("GroupSacrifice");
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" target: {target} squads: {squads:?}"));
            });
        }
        RequestCommand::GroupStopJob { player: _player, squads } => {
            ui.strong("GroupStopJob");
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" squads: {squads:?}"));
            });
        }
        RequestCommand::ModeChange { player: _player, target, mode } => {
            ui.strong("SuperWeaponShadowSetBombBuilding");
            ui.label(format!(" entity: {target} mode: {mode}"));
        }
        RequestCommand::Morph { player: _player, card_id, deck_position, tag, played_count, spell, target } => {
            ui.strong("Morph");
            ui.label(format!(" entity: {} card_id: {card_id} spell: {spell} deck_position: {deck_position} tag: {tag} played_count: {played_count}", target.targets[0].uid));
        }
        RequestCommand::PlayerLoot { account, loot_target, map_loot_id } => {
            ui.strong("Try Loot");
            ui.label(format!(" account: {account} loot_target: {loot_target} map_loot_id: {map_loot_id}")); // TODO map to _player name
        }
        RequestCommand::PlayerLootResult { account, loot_target, gold } => {
            ui.strong("Loot result");
            ui.label(format!(" account: {account} loot_target: {loot_target} gold: {gold:?}")); // TODO map to _player name
        }
        RequestCommand::PlayerSurrender { player: _player } => {
            ui.strong("Surrender");
        }
        RequestCommand::PortalDefineExitPoint { player: _player, portal, x, y } => {
            ui.strong("Define portal exit");
            ui.label(format!(" portal: {portal} x/y {x}/{y}"));
        }
        RequestCommand::PortalRemoveExitPoint { player: _player, portal } => {
            ui.strong("Remove portal exit");
            ui.label(format!(" portal: {portal}"));
        }
        RequestCommand::TunnelMakeExitPoint { player: _player, portal } => {
            ui.strong("Make portal exit");
            ui.label(format!(" portal: {portal}"));
        }
        RequestCommand::GroupUsePortal { squads, portal } => {
            ui.strong("GroupUsePortal");
            ui.horizontal_wrapped(|ui|{
                ui.label(format!(" portal: {portal} squads: {squads:?}"));
            });
        }
        RequestCommand::PowerSlotBuild { player: _player, slot } => {
            ui.strong("Build power slot");
            ui.label(format!(" slot: {slot}"));
        }
        RequestCommand::ProduceSquad { card, player: _player, card_position, tag, played_count, x, y, barrier_to_mount } => {
            ui.strong(format!("PLAY {:?}", CardTemplate::from_u32(*card)));
            ui.label(format!(" card: {card} barrier_to_mount: {barrier_to_mount} x/y: {x}/{y} tag: {tag} card_position: {card_position} played_count: {played_count}"));
        }
        RequestCommand::ScriptCheat { .. } => {
            ui.strong("ScriptCheat");
        }
        RequestCommand::ScriptVariable { checksum, value } => {
            ui.strong("ScriptVariable");
            ui.label(format!(" checksum: {checksum} value: {value}"));
        }
        RequestCommand::ScriptGoal { goal_sub_id } => {
            ui.strong("ScriptGoal");
            ui.label(format!(" goal_sub_id: {goal_sub_id}"));
        }
        RequestCommand::SquadRefill { player: _player, squad, auto_refil, cancel } => {
            ui.strong("SquadRefill");
            ui.label(format!(" squad: {squad} auto_refil: {auto_refil} cancel: {cancel}"));
        }
        RequestCommand::TokenSlotBuild { player: _player, slot, monument } => {
            ui.strong("Build monument");
            let monument = match monument {
                1 => "Shadow",
                2 => "Nature",
                3 => "Frost",
                4 => "Fire",
                5 => "All/mixed",
                _ => "invalid"
            };
            ui.label(format!(" slot: {slot} monument: {monument}"));
        }
        RequestCommand::UiInfo { player: _player, target, data1, data2 } => {
            ui.strong("Ping");
            let t = &target.targets[0];
            ui.label(format!(" data1: {data1} data2: {data2} x/y: {}/{}", t.x, t.y));
        }
        RequestCommand::ServerReplayEndOld { } => {
            ui.colored_label(Color32::RED, "Server replay end");
            ui.strong("following commands was ignored by server");
        }
        RequestCommand::SrAddition { .. } => {}
    }
}

fn player_from_action(action: &RequestCommand, player_by_id: &BTreeMap<u32, usize>, players: &[Player]) -> Option<u32> {
    match action {
        RequestCommand::ActivateCheat { player, .. } |
        RequestCommand::BarrierGateToggle { player, .. } |
        RequestCommand::BarrierSetBuildOrRepair { player, .. } |
        RequestCommand::BuildHouse { player, .. } |
        RequestCommand::CastSpellGod { player, .. } |
        RequestCommand::CastSpellGodMulti { player, .. } |
        RequestCommand::CastSpellEntity { player, .. } |
        RequestCommand::ConstructionRepair { player, .. } |
        RequestCommand::GroupAttack { player, .. } |
        RequestCommand::GroupEnterExitWall { player, .. } |
        RequestCommand::GroupGoto { player, .. } |
        RequestCommand::GroupHoldPosition { player, .. } |
        RequestCommand::GroupKillEntity { player, .. } |
        RequestCommand::SuperWeaponShadowSetBombBuilding { player, .. } |
        RequestCommand::GroupLootTarget { player, .. } |
        RequestCommand::GroupSacrifice { player, .. } |
        RequestCommand::GroupStopJob { player, .. } |
        RequestCommand::ModeChange { player, .. } |
        RequestCommand::Morph { player, .. } |
        RequestCommand::PlayerSurrender { player, .. } |
        RequestCommand::PortalDefineExitPoint { player, .. } |
        RequestCommand::PortalRemoveExitPoint { player, .. } |
        RequestCommand::TunnelMakeExitPoint { player, .. } |
        RequestCommand::PowerSlotBuild { player, .. } |
        RequestCommand::ProduceSquad { player, .. } |
        RequestCommand::ScriptCheat { player, .. } |
        RequestCommand::SquadRefill { player, .. } |
        RequestCommand::TokenSlotBuild { player, .. } |
        RequestCommand::UiInfo { player, .. } => { Some(*player) }
        RequestCommand::Desync { account, .. } |
        RequestCommand::PlayerLoot { account, .. } |
        RequestCommand::PlayerLootResult { account, .. } => {
            players
                .iter()
                .enumerate()
                .find(|(_, p)| p.id as u32 == *account)
                .and_then(|(i,_)| player_by_id.iter().find(|(_, index)| **index == i).map(|(id, _)| *id))
        }
        RequestCommand::GroupUsePortal { .. } |
        RequestCommand::ScriptVariable { .. } |
        RequestCommand::ScriptGoal { .. } |
        RequestCommand::ServerReplayEndOld { .. } |
        RequestCommand::SrAddition { .. } => { None }
    }
}

