mod replay;
mod validator;

use std::collections::BTreeMap;
use std::sync::atomic::AtomicU64;
use std::sync::atomic::Ordering::Relaxed;
use eframe::egui;
use eframe::egui::{Color32, DroppedFile, Id, RichText, Ui};
use egui_dock::{Tree};
use crate::app::replay::ReplayUiData;
use crate::app::validator::{ReplayValidatorConfig, ValidatedReplayUiData};

const VALIDATOR : &str = "Validator";

/// We derive Deserialize/Serialize so we can persist app state on shutdown.
#[derive(Default, serde::Deserialize, serde::Serialize)]
#[serde(default)] // if we add new fields, give them default values when deserializing old state
pub struct TemplateApp {
    validator: Option<ReplayValidatorConfig>,
    #[serde(skip)]
    replays : BTreeMap<u64, (String, ReplayData)>,
    #[serde(skip)]
    tabs : Tree<TabIndex>,
    #[serde(skip)]
    errors : Vec<(String, String)>,
    tabs_or_windows: bool,
    #[serde(skip)]
    game_data: Option<sr_libs::cff::GameDataCffFile>,
}

enum ReplayData {
    Validated(ValidatedReplayUiData),
    Raw(ReplayUiData)
}
impl ReplayData {
    fn show(&mut self, ui: &mut Ui) {
        match self {
            ReplayData::Validated(r) => r.show(ui),
            ReplayData::Raw(r) => r.show(ui),
        }
    }
}

impl TemplateApp {
    /// Called once before the first frame.
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        // Load previous app state (if any).
        // Note that you must enable the `persistence` feature for this to work.
        let mut app : Self = if let Some(storage) = cc.storage {
            eframe::get_value(storage, eframe::APP_KEY).unwrap_or_default()
        } else {
            Default::default()
        };

        #[cfg(target_arch = "wasm32")]
        if let Some(validator) = cc.integration_info.web_info.location.query_map.get(VALIDATOR) {
            if let Ok(validator) = serde_json::from_str(validator) {
                app.validator = Some(validator);
                if app.tabs_or_windows {
                    app.tabs.push_to_focused_leaf(TabIndex::Validator);
                }
            }
        }

        if app.validator.is_some() && app.tabs_or_windows {
            app.tabs.push_to_focused_leaf(TabIndex::Validator);
        }

        #[cfg(not(target_arch = "wasm32"))]
        {
            if let Some(game_data) = Some(include_bytes!("D:\\sr\\gamedata.cff")) {
            //if let Some(game_data) = include_optional::include_bytes_optional!("GameDataPATH") { // TODO why it does not work?
                let game_data = sr_libs::cff::GameDataCffFile::from_bytes(game_data).unwrap();
                app.game_data = Some(game_data);
            }
        }

        app
    }

    fn load_replay(&mut self, name: String, bytes: &[u8]) {
        match ReplayUiData::new(bytes) {
            Ok(replay) => {
                let id = ID.fetch_add(1, Relaxed);
                let replay = if let Some(validator) = &mut self.validator {
                    let validated_replay = validator.validate(replay, &self.game_data);
                    ReplayData::Validated(validated_replay)
                } else {
                    ReplayData::Raw(replay)
                };
                self.replays.insert(id, (name, replay));
                self.tabs.push_to_focused_leaf(TabIndex::Replay(id));
            }
            Err(e) =>
                self.errors.push((name, format!("{:?}", e))),
        }
    }

    fn on_file_drop(&mut self, file: &DroppedFile) {
        let name = if file.name.is_empty(){
            file.path
                .as_ref()
                .and_then(|p|p.file_name())
                .and_then(|n|n.to_str())
                .map(|n|n.to_owned())
                .unwrap_or_default()
        } else {
            file.name.clone()
        };
        if name.ends_with(".pmv") {
            if let Some(bytes) = &file.bytes {
                self.load_replay(name, bytes);
            } else if let Some(path) = &file.path {
                match std::fs::read(path) {
                    Ok(bytes) => self.load_replay(name, &bytes),
                    Err(e) =>
                        self.errors.push((name, format!("{:?}", e))),
                }
            } else {
                self.errors.push((name, "No data".to_owned()));
            }
        } else if name.ends_with(".cff") {
            if let Some(bytes) = &file.bytes {
                match sr_libs::cff::GameDataCffFile::from_bytes(bytes) {
                    Ok(game_data) => self.game_data = Some(game_data),
                    Err(e) => {
                        self.errors.push((name, format!("{e:?}")));
                    }
                }
            } else if let Some(path) = &file.path {
                match std::fs::read(path) {
                    Ok(bytes) => {
                        match sr_libs::cff::GameDataCffFile::from_bytes(&bytes) {
                            Ok(game_data) => self.game_data = Some(game_data),
                            Err(e) => self.errors.push((name, format!("{e:?}"))),
                        }
                    }
                    Err(e) => self.errors.push((name, format!("{e:?}"))),
                }
            } else {
                self.errors.push((name, "No data".to_owned()));
            }
        } else {
            self.errors.push((name, "Only replay files can be loaded".to_owned()));
        }
    }
    fn top_panel(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        egui::TopBottomPanel::top("top_panel").show(ctx, |ui| {
            // The top panel is often a good place for a menu bar:
            egui::menu::bar(ui, |ui| {
                #[cfg(not(target_arch = "wasm32"))] // no File->Quit on web pages!
                ui.menu_button("File", |ui| {
                    if ui.button("Quit").clicked() {
                        _frame.close();
                    }
                });
                if self.validator.is_none() && ui.button(VALIDATOR).clicked() {
                    self.validator = Some(ReplayValidatorConfig::new());
                    if self.tabs_or_windows {
                        self.tabs.push_to_focused_leaf(TabIndex::Validator);
                    }
                }
                let text = if self.tabs_or_windows {
                    "windows"
                } else {
                    "tabs"
                };
                if ui.button(text).clicked() {
                    self.tabs_or_windows = !self.tabs_or_windows;
                    if self.validator.is_some() {
                        if self.tabs_or_windows {
                            self.tabs.push_to_focused_leaf(TabIndex::Validator);
                        } else if let Some(tab) = self.tabs.find_tab(&TabIndex::Validator) {
                            self.tabs.remove_tab(tab);
                        }
                    }
                }
                ui.label("drop replay files anywhere on the app to load them");
            });
        });
    }
}

static ID : AtomicU64 = AtomicU64::new(1);

impl eframe::App for TemplateApp {
    /// Called each time the UI needs repainting, which may be many times per second.
    /// Put your widgets into a `SidePanel`, `TopPanel`, `CentralPanel`, `Window` or `Area`.
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        self.top_panel(ctx, frame);


        ctx.input(|input|{
            for file in &input.raw.dropped_files {
                self.on_file_drop(file);
            }
        });

        if self.tabs_or_windows {
            let style = egui_dock::Style::from_egui(ctx.style().as_ref());
            egui_dock::DockArea::new(&mut self.tabs)
                .style(style)
                .show(ctx, &mut TabViewer { replays: &mut self.replays, validator: &mut self.validator });
        } else {
            if let Some(validator) = &mut self.validator {
                let mut open = true;
                egui::Window::new(VALIDATOR).open(&mut open).show(ctx, |ui| {
                    validator.show(ui);
                });
                if !open {
                    self.validator = None;
                }
            }
            let mut remove = None;
            for (id, (name, replay)) in self.replays.iter_mut() {
                let mut open = true;
                egui::Window::new(&*name).id(Id::new(id)).open(&mut open).show(ctx, |ui| {
                    replay.show(ui);
                });
                if !open {
                    remove = Some(*id)
                }
            }
            if let Some(id) = remove {
                self.replays.remove(&id);
            }
        }

        if !self.errors.is_empty() {
            let mut open = true;
            egui::Window::new("Errors").open(&mut open).show(ctx, |ui| {
                egui::Grid::new("Errors").show(ui, |ui|{
                    ui.label(RichText::new("file").strong());
                    ui.label(RichText::new("error").strong());
                    ui.label("clean");
                    ui.end_row();
                    let mut remove = None;
                    for (i, (name, error)) in self.errors.iter().enumerate() {
                        ui.label(name);
                        ui.label(error);
                        if ui.button(RichText::new("X").color(Color32::RED)).clicked() {
                            remove = Some(i);
                        }
                        ui.end_row();
                    }
                    if let Some(remove) = remove {
                        self.errors.remove(remove);
                    }
                });
            });
            if !open {
                self.errors.clear();
            }
        }

        egui::TopBottomPanel::bottom("bottom").show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.hyperlink_to("Report a bug with tag @Kubik", "https://discord.com/channels/173414671678832640/801219974177751050");
                ui.hyperlink_to("Or directly at gitlab (where you can also check the code", "https://gitlab.com/xxxkubikx/replay_info");
            });
            #[cfg(target_arch = "wasm32")]
            ui.hyperlink_to("Windows EXE", "https://jakub.bandola.cz/wa/replay_info/replay_info_gui.exe");
        });
    }

    /// Called by the frame work to save state before shutdown.
    fn save(&mut self, storage: &mut dyn eframe::Storage) {
        eframe::set_value(storage, eframe::APP_KEY, self);
    }
}

struct TabViewer<'a> {
    validator: &'a mut Option<ReplayValidatorConfig>,
    replays : &'a mut BTreeMap<u64, (String, ReplayData)>,
}

impl<'a> egui_dock::TabViewer for TabViewer<'a> {
    type Tab = TabIndex;

    fn ui(&mut self, ui: &mut Ui, tab: &mut Self::Tab) {
        match tab {
            TabIndex::Validator =>
                if let Some(validator) = self.validator {
                    validator.show(ui)
                }
            TabIndex::Replay(tab) => if let Some(tab) = self.replays.get_mut(tab) {
                tab.1.show(ui);
            } else {
                // closing... this is the last frame
            }
        }
    }

    fn title(&mut self, tab: &mut Self::Tab) -> egui::WidgetText {
        match tab {
            TabIndex::Validator => VALIDATOR.to_owned().into(),
            TabIndex::Replay(tab) => if let Some(tab) = self.replays.get(tab) {
                tab.0.clone().into()
            } else {
                // closing... this is the last frame
                "CLOSING".to_owned().into()
            }
        }
    }
    fn on_close(&mut self, tab: &mut Self::Tab) -> bool {
        match tab {
            TabIndex::Validator => {
                *self.validator = None;
            }
            TabIndex::Replay(tab) => {
                self.replays.remove(tab);
            }
        }
        true
    }
}

#[derive(Eq, PartialEq)]
pub enum TabIndex {
    Validator,
    Replay(u64)
}
