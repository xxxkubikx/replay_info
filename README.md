# replay info

Just simple example how to use the ``replay_info`` library

## How to contribute

Thank you for your interest in contributing to the replay_info.

Just report a bug or create a merge request.

### Run Locally

just simple ``cargo run --release`` or ``cargo run`` for debug

### Web Deploy
run following script:
> $env:RUSTFLAGS='--cfg=web_sys_unstable_apis'; trunk build --release --public-url YOUR_RELATIVE_PATH

and replace ``YOUR_RELATIVE_PATH`` with for example ``wa/replay_info/`` in my case of https://jakub.bandola.cz/wa/replay_info/ 
